# TEST TASK

Приложение разработано на drf для прохождения тестового задания. В качестве СУБД используется sqlite3, поэтому в докере нет контейнера bd. При необходимости можно настроить работу с другими СУБД, добавив переменные окружения и внеся изменения в settings.py

## Установка и запуск

1. Склонировать репозиторий с Github:

````
git clone git@gitlab.com:Lumbril/test-task-solution-factory.git
````

2. Перейти в директорию проекта
3. Создать виртуальное окружение:

````
python -m venv venv
````

4. Активировать окружение: 

````
source \venv\bin\activate
````
5. Файл .envEx переименовать в .env и изменить данные в нем на подходящие вам 
6. Установка зависимостей:

```
pip install -r requirements.txt
```

7. Создать и применить миграции в базу данных:
```
python manage.py makemigrations
python manage.py migrate
```
8. Запустить сервер
```
python manage.py runserver
```
9. Запустить celery
```
celery -A core worker -l info -P solo
```
10. Запустить flower

```
celery -A core flower --port=5555
```

***
## Установка проекта с помощью docker-compose


1. Склонировать репозиторий с Github
```
git clone git@gitlab.com:Lumbril/test-task-solution-factory.git
```
2. Перейти в директорию проекта
3. Файл .envEx переименовать в .env и изменить данные в нем на подходящие вам 
4. Запустить контейнеры 
``` 
docker-compose up -d
 ```
5. Остановка работы контейнеров 
```
docker-compose stop
```
***
```http://0.0.0.0:8000/api/v1/``` - api проекта  
```http://0.0.0.0:8000/api/v1/clients/``` - клиенты  
```http://0.0.0.0:8000/api/v1/clients/<pk>/``` - детальная информация о клиенте  
```http://0.0.0.0:8000/api/v1/mailinglist/``` - рассылки  
```http://0.0.0.0:8000/api/v1/mailinglist/<pk>/``` - детальная статистика по конкретной рассылке  
```http://0.0.0.0:8000/docs/``` - docs проекта, оформленный через swagger  
```http://0.0.0.0:5555``` - celery flower
