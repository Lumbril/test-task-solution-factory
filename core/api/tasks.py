import datetime
import os

import pytz
import requests
from django.shortcuts import get_object_or_404

from api.models import MailingList, Client, Message
from core.celery import app


@app.task(bind=True, retry_backoff=True)
def send_message(self, data, mailing_id, client_id):
    token = os.getenv('TOKEN_OF_SERVICE')
    url = os.getenv('URL_OF_SERVICE')
    mailing = get_object_or_404(MailingList, id=mailing_id)
    client = get_object_or_404(Client, id=client_id)

    timezone = pytz.timezone(client.timezone)
    now = datetime.datetime.now(timezone).astimezone(pytz.UTC)
    start_mailing = mailing.datetime_start.replace(tzinfo=timezone).astimezone(pytz.UTC)
    end_mailing = mailing.datetime_end.replace(tzinfo=timezone).astimezone(pytz.UTC)

    if start_mailing <= now <= end_mailing:
        header = {
            'Authorization': f'Bearer {token}',
            'Content-Type': 'application/json'}
        try:
            requests.post(url=url + str(data['id']), headers=header, json=data)
        except requests.exceptions.RequestException as exc:
            raise self.retry(exc=exc)
        else:
            Message.objects.filter(pk=data['id']).update(status=True)
    else:
        time = (start_mailing - now).seconds

        return self.retry(countdown=time + 1)
