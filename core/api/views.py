from django.shortcuts import get_list_or_404, get_object_or_404
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.exceptions import ValidationError
from rest_framework.views import APIView
from rest_framework.response import Response
from . import models, serializers


class GetListOrCreateClientView(APIView):
    """
        API for get all clients with or without filters or add client to DB by serializer
    """

    @swagger_auto_schema(
        operation_id='Получить список всех клиентов',
        responses={'200': serializers.ClientSerializer(many=True)},
    )
    def get(self, request):
        query_params = {field: value for field, value in request.query_params.items()}

        clients = get_list_or_404(models.Client, **query_params)
        serializer = serializers.ClientSerializer(clients, many=True)

        validated_data = serializer.data

        return Response(data=validated_data, status=status.HTTP_200_OK)

    @swagger_auto_schema(
        operation_id='Создать нового клиента',
        responses={'200': serializers.ClientSerializer()},
        request_body=serializers.CreateClientSerializer,
        operation_description=
        """
            Example of POST method:
                {
                    "phone": "79999999999",
                    "tag": "qwerty",
                    "timezone": "Europe/Moscow"
                }
        """,
    )
    def post(self, request):
        serializer = serializers.CreateClientSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        validated_data = serializer.data
        serializer.create(validated_data)

        return Response(data=validated_data, status=status.HTTP_200_OK)


class UpdateOrDeleteClientView(APIView):
    """API для получения, обновления и удаления клиента по id"""

    @swagger_auto_schema(
        operation_id='Получить клиента по id',
        responses={'200': serializers.ClientSerializer()},
    )
    def get(self, request, client_id):
        client = get_object_or_404(models.Client, id=client_id)
        serializer = serializers.ClientSerializer(client, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)

        validated_data = serializer.data

        return Response(data=validated_data, status=status.HTTP_200_OK)

    @swagger_auto_schema(
        operation_id='Обновить данные клиента',
        responses={'200': serializers.ClientSerializer()},
        request_body=serializers.CreateClientSerializer,
        operation_description=
        """
        Example of PUT method:
            {
            "phone": "79831575107",
            "code_of_mobile_operator": 983,
            "tag": "qwerty",
            "timezone": "Europe/Moscow"
            }
        OR
            {
            "tag": "qwerty"
            }
        """,
    )
    def put(self, request, client_id):
        data = request.data
        client = get_object_or_404(models.Client, id=client_id)

        for field, value in data.items():
            if hasattr(client, field):
                setattr(client, field, value)
            else:
                raise ValidationError('Неверные параметры')

        client.save()

        serializer = serializers.ClientSerializer(client, data=data, partial=True)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.data

        return Response(data=validated_data, status=status.HTTP_200_OK)

    @swagger_auto_schema(
        operation_id='Удалить клиента',
        responses={'200': 'success'},
    )
    def delete(self, request, client_id):
        client = get_object_or_404(models.Client, id=client_id)
        client.delete()

        return Response(data={'message': 'success'}, status=status.HTTP_200_OK)


class GetListOrCreateMailing(APIView):
    """
        API для получения списка рассылок и создания рассылки
    """

    @swagger_auto_schema(
        operation_id='Получить все рассылки',
        responses={'200': serializers.MailingListSerializer(many=True)},
    )
    def get(self, request):
        mailing_list = get_list_or_404(models.MailingList)
        serializer = serializers.MailingListSerializer(mailing_list, many=True)
        validated_data = serializer.data

        return Response(data=validated_data, status=status.HTTP_200_OK)

    @swagger_auto_schema(
        operation_id='Создать новую рассылку',
        responses={'200': serializers.MailingListSerializer()},
        request_body=serializers.MailingListSerializer,
        operation_description=
        """
            Example of POST method:
                {
                    "datetime_of_start_mailing": "2022-08-09 20:00:00",
                    "text": "some text, which would be sent to clients",
                    "filter": {
                                "tag": "qwerty", 
                                "code_mobile_operator": "983"
                            },
                    "datetime_of_end_mailing": "2022-08-09 23:00:00"
                }
        """,
    )
    def post(self, request):
        serializer = serializers.MailingListSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data
        serializer.create(validated_data)

        return Response(data=validated_data, status=status.HTTP_200_OK)


class UpdateOrDeleteMailingView(APIView):
    """
        API для получения, редактирования и удаления рассылки
    """

    @swagger_auto_schema(
        operation_id='Получить рассылку по id',
        responses={'200': serializers.MailingListSerializer()},
    )
    def get(self, request, mailing_id):
        mailing = get_object_or_404(models.MailingList, id=mailing_id)
        serializer = serializers.MailingListSerializer(mailing, data=request.data, partial=True)

        serializer.is_valid(raise_exception=True)

        validated_data = serializer.data

        return Response(data=validated_data, status=status.HTTP_200_OK)

    @swagger_auto_schema(
        operation_id='Изменить рассылку',
        responses={'200': serializers.MailingListSerializer()},
        request_body=serializers.MailingListSerializer,
        operation_description=
        """
         Example of PUT method:
                {
                    "datetime_of_start_mailing": "2022-08-09T20:00:00Z",
                    "text": "some text, which would be sent to clients",
                    "filters": {
                        "tag": "qwerty",
                        "code_mobile_operator": "983"
                    },
                    "datetime_of_end_mailing": "2022-08-09T23:00:00Z",
                }
            OR
                {
                "text": "some text, which would be sent to clients",
                "filters": {
                    "tag": "qwerty",
                    "code_of_mobile_operator": "983"
                    }
                }
        """,
    )
    def put(self, request, mailing_id):
        data = request.data
        mailing = get_object_or_404(models.MailingList, id=mailing_id)

        for field, value in data.items():
            if hasattr(mailing, field):
                setattr(mailing, field, value)
            else:
                raise ValidationError('Неверные параметры')

        mailing.save()

        serializer = serializers.MailingListSerializer(mailing, data=data, partial=True)

        serializer.is_valid(raise_exception=True)

        validated_data_of_mailing = serializer.data

        return Response(data=validated_data_of_mailing, status=status.HTTP_200_OK)

    @swagger_auto_schema(
        operation_id='Удалить рассылку',
        responses={'200': 'success'},
    )
    def delete(self, request, mailing_id):
        mailing = get_object_or_404(models.MailingList, id=mailing_id)
        mailing.delete()

        return Response(data={'message': 'success'}, status=status.HTTP_200_OK)
