from django.db.models import Q
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.shortcuts import get_object_or_404
import pytz

from api import serializers
from api.models import MailingList, Client, Message
from api.tasks import send_message


@receiver(post_save, sender=MailingList, dispatch_uid='put_message_on_task_queue')
def put_message_on_task_queue(sender, instance, created, **kwargs):
    if created:
        tag = instance.filter.get('tag')
        code_mobile_operator = instance.filter.get('code_mobile_operator')
        clients = Client.objects.filter(Q(tag=tag) | Q(code_mobile_operator=code_mobile_operator))

        for client in clients:
            message_data = {
                'mailing': instance.id,
                'client': client.id
            }

            message_serializer = serializers.MessageSerializer(data=message_data)
            message_serializer.is_valid(raise_exception=True)

            validated_data = message_serializer.validated_data
            message_serializer.create(validated_data=validated_data)

            message = get_object_or_404(Message, mailing=instance.id, client=client.id)
            massage_id = message.id
            phone = client.phone
            text = instance.text

            data = {
                "id": massage_id,
                "phone": phone,
                "text": text
            }

            timezone = pytz.timezone(client.timezone)
            send_message.apply_async(
                (data, instance.id, client.id),
                eta=instance.datetime_start.replace(tzinfo=timezone).astimezone(pytz.UTC),
                expires=instance.datetime_end.replace(tzinfo=timezone).astimezone(pytz.UTC)
            )
