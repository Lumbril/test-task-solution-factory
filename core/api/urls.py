from . import views
from django.urls import path


urlpatterns = [
    path('clients/', views.GetListOrCreateClientView.as_view(), name='client-create'),
    path('clients/<client_id>', views.UpdateOrDeleteClientView.as_view(), name='client-update-delete'),
    path('mailinglist/', views.GetListOrCreateMailing.as_view(), name='mailing-create'),
    path('mailinglist/<mailing_id>', views.UpdateOrDeleteMailingView.as_view(), name='mailing-update-delete'),
]