from django.db import models
from django.core.validators import RegexValidator
import pytz


class MailingList(models.Model):
    text = models.TextField(verbose_name='Текст сообщения')
    filter = models.JSONField(verbose_name='Фильтр свойств клиентов')
    datetime_start = models.DateTimeField(verbose_name='Дата и время запуска рассылки')
    datetime_end = models.DateTimeField(verbose_name='Дата и время окончания рассылки')

    class Meta:
        db_table = 'mailing_list'
        verbose_name = 'Рассылка'
        verbose_name_plural = 'Рассылки'


class Client(models.Model):
    phone = models.CharField(max_length=11, unique=True,
                             validators=[RegexValidator(
                                regex=r'7\d{10}',
                                message="Номер телефона клиента должен быть в формате 7XXXXXXXXXX (X - число от 0 до 9)"
                                )],
                             verbose_name='Номер телефона')
    code_mobile_operator = models.CharField(max_length=3, verbose_name='Код мобильного оператора')
    tag = models.CharField(max_length=10, verbose_name='Тэг')
    timezone = models.CharField(max_length=100,
                                choices=tuple(zip(pytz.all_timezones, pytz.all_timezones)),
                                verbose_name='Часовой пояс')

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.code_mobile_operator = str(self.phone)[1:4]

        return super().save(force_insert, force_update, using, update_fields)

    class Meta:
        db_table = 'clients'
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'


class Message(models.Model):
    date_created = models.DateTimeField(auto_now=True, verbose_name='Дата и время отправки')
    status = models.BooleanField(default=False, verbose_name='Статус отправки')
    mailing = models.ForeignKey(MailingList, null=True, on_delete=models.SET_NULL, verbose_name='id рассылки',
                                related_name='mailing')
    client = models.ForeignKey(Client, null=True, on_delete=models.SET_NULL, verbose_name='id клиента',
                               related_name='client')

    class Meta:
        db_table = 'messages'
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'
